# Bitcoin Daily Prediction

Technical analysis tools and datasets that aim to predict Bitcoin market movements

## Results ##

| Type         | Total periods | Correct predictions | Correct % |
|--------------|---------------|--------------|------------------|
| `macd-basic` | 2308 | 1172 | 50.78 |
| `macd-basic-tuned` | 2298 | 1477 | 64.27 |
| `trend-continuation` | 2341 | 1104 | 47.16 |
| `rsi-basic` | 2328 | 1224 | 52.58 | 
| `rsi-basic-tuned` | 2318 | 1226 | 52.89 |
| `cmo-basic` | 2328 | 1224 | 52.58 |
| `cmo-basic-tuned` | 2318 | 1226 | 52.89 |
| `tsf-basic` | 2328 | 1155 | 49.61 |
| `tsf-basic-tuned` | 2105 | 1141 | 54.2 |
