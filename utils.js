const fs = require('fs');
const path = require('path');

const csv = require('@fast-csv/parse');
const Decimal = require('decimal.js');

// Default source file
const sourceFile = path.resolve(__dirname, 'datasets', 'cryptochassis-coinbase-btc-usd-ohlc-1451606400-1653955200.csv');

let rows = [];

module.exports = {
    getRows(callback) {

        fs.createReadStream(sourceFile)
            .pipe(csv.parse({ headers: true }))
            .on('error', error => console.error(error))
            .on('data', row => {
                rows.push(row);
            })
            .on('end', () => {
                callback(rows);
            });

    },
    outputResults(periods, correctPredictions) {
        console.log([
            periods,
            correctPredictions,
            Decimal(correctPredictions).div(periods).mul(100).toDP(2),
            ""
        ].join(" | "));
    }
};
