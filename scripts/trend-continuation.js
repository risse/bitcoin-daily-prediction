/**
 *
 * Trend continuation
 * Super simple trend continuation, aka if yesterday was positive, today will be positive too
 */

const Decimal = require('decimal.js');

const { getRows, outputResults } = require('../utils');

let periods = 0;
let correctPredictions = 0;

getRows(rows => {

    let closes = rows.map(row => row.close);

    let previousClose = undefined;

    closes.forEach((close, index) => {

        if(closes[index + 1] !== undefined && previousClose !== undefined) {

            periods++;

            // Actual direction that the price went (aka correct value)
            var actualDirection = Decimal(closes[index + 1]).minus(close);

            // Yesterday's direction
            var yesterdayDirection = Decimal(close).minus(previousClose);

            if(yesterdayDirection.gte(0) && actualDirection.gte(0)) {
                correctPredictions++;
            } else if(yesterdayDirection.lt(0) && actualDirection.lt(0)) {
                correctPredictions++;
            }

        }

        previousClose = close;

    });

    outputResults(periods, correctPredictions);

});
