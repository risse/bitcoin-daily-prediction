/**
 *
 * Basic RSI tuned prediction
 * Similar to RSI basic but tunes the period value. Looks at RSI direction and compares it to the actual price direction
 */

const talib = require('talib');
const Decimal = require('decimal.js');

const { getRows, outputResults } = require('../utils');

// Use RSI default values
const rsiPeriod = 24;

let periods = 0;
let correctPredictions = 0;

getRows(rows => {

    let closes = rows.map(row => row.close);

    var rsi = talib.execute({
        name: "RSI",
        startIdx: 0,
        endIdx: closes.length - 1,
        inReal: closes,
        optInTimePeriod: rsiPeriod
    });

    // Cut closes from the beginning where there's no data
    closes = closes.slice(rsi.begIndex);

    rsi.result.outReal.forEach((rsiValue, index) => {

        if(closes[index + 1] !== undefined) {

            periods++;

            // Actual direction that the price went (aka correct value)
            var actualDirection = Decimal(closes[index + 1]).minus(closes[index]);

            if(rsiValue >= 50 && actualDirection.gte(0)) {
                correctPredictions++;
            } else if(rsiValue < 50 && actualDirection.lt(0)) {
                correctPredictions++;
            }

        }

    });

    outputResults(periods, correctPredictions);

});
