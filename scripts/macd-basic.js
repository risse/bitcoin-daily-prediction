/**
 *
 * Basic MACD signal prediction
 * Looks at MACD signal direction and compares it to the actual price direction
 */

const talib = require('talib');
const Decimal = require('decimal.js');

const { getRows, outputResults } = require('../utils');

// Use MACD default values
const macdFastPeriod = 12;
const macdSlowPeriod = 26;
const macdSignalPeriod = 9;

let periods = 0;
let correctPredictions = 0;

getRows(rows => {

    let closes = rows.map(row => row.close);

    var macd = talib.execute({
        name: "MACD",
        startIdx: 0,
        endIdx: closes.length - 1,
        inReal: closes,
        optInFastPeriod: macdFastPeriod,
        optInSlowPeriod: macdSlowPeriod,
        optInSignalPeriod: macdSignalPeriod
    });

    // Cut closes from the beginning where there's no data
    closes = closes.slice(macd.begIndex);

    let previousSignal = undefined;

    macd.result.outMACDSignal.forEach((macdSignal, index) => {

        if(closes[index + 1] !== undefined && previousSignal !== undefined) {

            periods++;

            // Actual direction that the price went (aka correct value)
            var actualDirection = Decimal(closes[index + 1]).minus(closes[index]);

            // Direction that the MACD signal is pointing towards
            var signalDirection = Decimal(macdSignal).minus(previousSignal);

            if(signalDirection.gte(0) && actualDirection.gte(0)) {
                correctPredictions++;
            } else if(signalDirection.lt(0) && actualDirection.lt(0)) {
                correctPredictions++;
            }

        }

        previousSignal = macdSignal;

    });

    outputResults(periods, correctPredictions);

});
