/**
 *
 * Basic CMO prediction
 * Looks at CMO direction and compares it to the actual price direction
 */

const talib = require('talib');
const Decimal = require('decimal.js');

const { getRows, outputResults } = require('../utils');

// Use CMO default values
const cmoPeriod = 24;

let periods = 0;
let correctPredictions = 0;

getRows(rows => {

    let closes = rows.map(row => row.close);

    var cmo = talib.execute({
        name: "CMO",
        startIdx: 0,
        endIdx: closes.length - 1,
        inReal: closes,
        optInTimePeriod: cmoPeriod
    });

    // Cut closes from the beginning where there's no data
    closes = closes.slice(cmo.begIndex);

    cmo.result.outReal.forEach((cmoValue, index) => {

        if(closes[index + 1] !== undefined) {

            periods++;

            // Actual direction that the price went (aka correct value)
            var actualDirection = Decimal(closes[index + 1]).minus(closes[index]);

            if(cmoValue >= 0 && actualDirection.gte(0)) {
                correctPredictions++;
            } else if(cmoValue < 0 && actualDirection.lt(0)) {
                correctPredictions++;
            }

        }

    });

    outputResults(periods, correctPredictions);

});
