/**
 *
 * Basic TSF prediction
 * Looks at TSF direction and compares it to the actual price direction
 */

const talib = require('talib');
const Decimal = require('decimal.js');

const { getRows, outputResults } = require('../utils');

// Use TSF default values
const tsfPeriod = 237;

let periods = 0;
let correctPredictions = 0;

getRows(rows => {

    let closes = rows.map(row => row.close);

    var tsf = talib.execute({
        name: "TSF",
        startIdx: 0,
        endIdx: closes.length - 1,
        inReal: closes,
        optInTimePeriod: tsfPeriod
    });

    // Cut closes from the beginning where there's no data
    closes = closes.slice(tsf.begIndex);

    let previousTsf = undefined;

    tsf.result.outReal.forEach((tsfValue, index) => {

        if(closes[index + 1] !== undefined && previousTsf !== undefined) {

            periods++;

            // Actual direction that the price went (aka correct value)
            var actualDirection = Decimal(closes[index + 1]).minus(closes[index]);

            // Direction that the TSF is pointing towards
            var tsfDirection = Decimal(tsfValue).minus(previousTsf);

            if(tsfDirection.gte(0) && actualDirection.gte(0)) {
                correctPredictions++;
            } else if(tsfDirection.lt(0) && actualDirection.lt(0)) {
                correctPredictions++;
            }

        }

        previousTsf = tsfValue;

    });

    outputResults(periods, correctPredictions);

});
